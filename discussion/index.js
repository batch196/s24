// console.log('hello');

// es6 updates. is one of the major latest versions of JS

//let, const - are the new standards of creating variables
//var was the keyword to create variables before es6

//exponent operator

	//older way
let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3)

	//ES6 way (**)
let fivePowerOf2 = 5**2;
console.log(fivePowerOf2);

let fivePowerOf4 = 5**4;
console.log(fivePowerOf4);

let squareRootOf4 = 4**.5;
console.log(squareRootOf4);

let string1 = 'Javascript';
let string2 = 'not';
let string3 = 'is';
let string4 = 'Typescript';
let string5 = 'Java';
let string6 = 'Zuitt';
let string7 = 'Conding';
let string8 = 'Bootcamp';

//Mini activity

// let sentence1 = string1 +' '+ string3 +' '+ string2 +' '+ string5;
// let sentence2 = string4 + ' '+string3 +' '+string1;

// console.log(sentence1);
// console.log(sentence2);

//template literals
//"",'' - string literals
//Template Literals allows us to create stings using `` and easily embeed JS expression in it

let sentence1 = `${string1}${string3}${string2}${string5}`;
let sentence2 = `${string4} ${string3}${string1}`;

console.log(sentence1);
console.log(sentence2);

//use template literal to add new sentence variable
let sentence3 = `${string6} ${string7} ${string8}`;
console.log(sentence3);

let sentence4 = `The sum of 15 and 25 is ${15+25}`;
console.log(sentence4);

let person ={

	name     : 'Michael',
	position: 'Developer',
	income   : 50000,
	expenses : 60000

}

console.log(`${person.name} is a ${person.position}`)
console.log(`his income is ${person.income} and expenses at ${person.expenses}. his current balance is ${person.income - person.expenses}`);

//Destructuring arrays and Objects
//destructuring will allow us to save arrat items or objects properties into new variables without having to create/initialize with accessing the items/properties

let array1 = ['Curry','Lillard','Paul','Irving'];
// let player1 = array1[0];
// let player2 = array1[1];
// let player3 = array1[2];
// let player4 = array1[3];
// console.log(player1,player2,player3,player4);

//array destructuring is when we save array items into variable

let [player1,player2,player3,player4] = array1
console.log(player1,player2,player3,player4);

let array2 = ['Jokic','Embiid','Howard','Anthony-Towns'];
let [center1,center2,,center4] = array2;
console.log(center4);

//object destructuring - the order of destructuring does not matter. however, the name of the variable does not match a property in the object

let pokemon1 = {
	name: 'Bulbasaur',
	type: 'Grass',
	level: 10,
	moves: ['Razor Leaf','Tackle','Leech Seed']
}
let {level,type,name,moves,personality} = pokemon1;

console.log(level);
console.log(type);
console.log(moves);
console.log(personality);

let pokemon2 = {
	name: 'Charmander',
	type: 'Fire',
	level: 11,
	moves: ['Ember','Scratch']
}
const{name: name2} = pokemon2;
console.log(name2);

//arrow functions are an alternative way of wringing functions in Javascript. However, there are pros and cons between traditional arrow function.

// traditional function
function displayMsg(){
	console.log("hello");
}
displayMsg();

//arrow function
const hello = () => {
	console.log('hello from arrow');
}
hello();

const greet = (friend) => {
	console.log(`Hi ${friend.name}`);
	// console.log(friend);
}
greet(person);

// Arrow vs traditional function
//implicit return - allows to re
// function addNum(num1,num2){
// console.log(num1,num2);
// return num1+num2;

// }
// let sum = addNum(5,10);
// console.log(sum);

//arrow func
let subNum = (num1,num2) => num1 - num2;
let difference = subNum(10,5);
console.log(difference);

//implicit return will only work on arrow function without {}
//{} - is for multilines function


//mini acitivity
let addNum = (num1,num2) => num1+num2;

let sum = addNum(50,70);
console.log(sum);

//traditional functions vs arrow functions as methods

let character1 = {
	name: 'Cloud Strife',
	occupation: 'soldier',
	greet : function(){
		console.log(this);
		console.log(`Hi! im ${this.name}`);
	},
	introduceJob: () => {
		//in an arrow function as method,
		//the .this keyword will not refer to the current object. instead it will refer to the global
		console.log(this);
	}
}
character1.greet();
character1.introduceJob();

//class based object blueprints
	//In JS, classes are templates of objects
	//we can create objects out of classes
	//before the introduction of classes in JS, we mimic this behavior or being able
	//to create objects

	function Pokemon(name,type,level){
		this.name = name;
		this.type = type;
		this.level = level;
	}
	//with the advent of ES6, we are now introduced to a special method of creating initializing an object
	//classes - PascalCasing
	//normal function - camelCasing

	class Car {
		constructor(brand,name,year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}
let car1 = new Car('toyota','Vios','2002');
let car2 = new Car('Cooper','Mini','1969');
let car3 = new Car('Porche','911','1967');

console.log(car1);
console.log(car2);
console.log(car3);

//mini activity

class Pokemon2 {
	constructor(name,type,level){
		this.name = name;
		this.type = type;
		this.level = level;
		console.log(this);
	}
}

let pokemonA = new Pokemon2('PokemonA','Grass',20);
let pokemonB = new Pokemon2('PokemonB','Grass',25);
let pokemonC = new Pokemon2('PokemonC','Grass',30);
















